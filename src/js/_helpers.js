window.b2b1_tabletBreakpoint = 768;
window.b2b1_desktopBreakpoint = 1200;
window.b2b1_themeRoot = null;
window.b2b1_isMainPage = window.location.pathname === '/';
window.b2b1_supportsGrid = typeof document.createElement('div').style.grid === 'string'; // может modernizr?
window.b2b1_isMobile = () => window.innerWidth < b2b1_tabletBreakpoint;
window.b2b1_isTablet = () => window.innerWidth >= b2b1_tabletBreakpoint && window.innerWidth < b2b1_desktopBreakpoint;
window.b2b1_isDesktop = () => !b2b1_isMobile() && !b2b1_isTablet();

const $ = window.jQuery = require('jquery');

$.fn.hasScrollBar = function() {
  return this.get(0).scrollHeight > this.height();
}

/**
 * Check if there is scrollbar
 * https://stackoverflow.com/a/4814526
 */
$.fn.hasScrollBar = function() {
  return this.get(0).scrollHeight > this.height();
}

/**
 * .resizeend method for debounce
 */
$.resizeend = function(el, options){
  var base = this;

  base.$el = $(el);
  base.el = el;

  base.$el.data("resizeend", base);
  base.rtime = new Date(1, 1, 2000, 12,0,0);
  base.timeout = false;
  base.delta = 200;

  base.init = function(){
    base.options = $.extend({},$.resizeend.defaultOptions, options);

    if(base.options.runOnStart) base.options.onDragEnd();

    $(base.el).resize(function() {

      base.rtime = new Date();
      if (base.timeout === false) {
        base.timeout = true;
        setTimeout(base.resizeend, base.delta);
      }
    });
  };
  base.resizeend = function() {
    if (new Date() - base.rtime < base.delta) {
      setTimeout(base.resizeend, base.delta);
    } else {
      base.timeout = false;
      base.options.onDragEnd();
    }
  };

  base.init();
};
$.resizeend.defaultOptions = {
  onDragEnd : function() {},
  runOnStart : false
};
$.fn.resizeend = function(options){
  return this.each(function(){
    (new $.resizeend(this, options));
  });
};

/**
 * Проверить есть ли родитель с таким селектором
 */
$.fn.hasParent = function(selector) {
  return !!$(this).parents(selector).length
};
