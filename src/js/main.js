window.$ = window.jQuery = require('jquery');
require('./_helpers.js');

require('bootstrap/js/dist/collapse');//themes\octoportus\node_modules\bootstrap\js\src\collapse.js
require('bootstrap/js/dist/modal');

$(document).on('ajaxSetup', function(event, context) {
    // Enable AJAX handling of Flash messages on all AJAX requests
    context.options.flash = true;

    // Enable the StripeLoadIndicator on all AJAX requests
    context.options.loading = $.oc.stripeLoadIndicator;

    // Handle Error Messages by triggering a flashMsg of type error
    context.options.handleErrorMessage = function(message) {
        $.oc.flashMsg({ text: message, class: 'error' });
    };

    // Handle Flash Messages by triggering a flashMsg of the message type
    context.options.handleFlashMessage = function(message, type) {
        $.oc.flashMsg({ text: message, class: type });
    };
})

$(function() {
    $(document).on('click', '.cargos__delete', function(e) {
        const btn = e.target;

        $(btn).request('onDeleteCargo', {
            data: {
                id: btn.dataset.id,
            },
            success(data) {
                $(btn).closest('.cargos__item').remove();

                if ($('.cargos__list .cargos__item').length === 0) {
                    $('.cargos__list > .cargos__no-data').show();
                }
            },
        });
    });

    $modal = $('#modal');

    $(document).on('click', '.cargos__get-form', function() {
        $(this).request('onGetCargoForm', {
            data: {
                id: this.dataset.id,
                order_id: this.dataset.orderId,
            },
            success(data) {
                $modal.find('.modal-title').text(data.title)
                $modal.find('.modal-body').append(data.cargoForm);
                $modal.modal();
            },
        });
    });

    $modal.on('submit', '#update-cargo-form', function(e) {
        e.preventDefault();

        const isUpdate = typeof this.dataset.id !== 'undefined';

        // Если новая заявка
        if (!isUpdate) {
            $(this).request('onCreateCargo', {
                data: {
                    order_id: this.dataset.orderId,
                },
                success(data) {
                    $modal.modal('hide');
                    $('.cargos__list > .cargos__no-data').hide();
                    $('.cargos__list .collapse.show').collapse('hide');
                    $('.cargos__list').append(data.cargoItem);
                },
                fail(err) {
                    console.error(err);
                },
            });

        } else { // Если редактирование
            const cargoId = this.dataset.id;

            $(this).request('onUpdateCargo', {
                data: {
                    id: cargoId,
                    order_id: this.order_id,
                },
                success(data) {
                    $modal.modal('hide');

                    const $oldCargoItem = $('.cargos')
                        .find(`.cargos__get-form[data-id="${cargoId}"]`)
                        .closest('.cargos__item');

                    $oldCargoItem.replaceWith(data.cargoItem);
                },
                fail(err) {
                    console.error(err);
                },
            });
        }
    });

    $('.orders__get-form, .get-order-form').on('click', function() {
        $(this).request('onGetOrderForm', {
            data: {
                id: this.dataset.id,
            },
            success(data) {
                $modal.find('.modal-title').text(data.title)
                $modal.find('.modal-body').append(data.orderForm);
                $modal.modal();
            },
        });
    });

    $modal.on('submit', '#update-order-form', function(e) {
        e.preventDefault();

        const data = {
            id: this.dataset.id,
            key: this.dataset.key,
        };

        // Если новая заявка
        if (typeof data.id === 'undefined') {
            $(this).request('onCreateOrder', {
                data,
                success(data) {
                    // Сразу перейти к редактированию
                    document.location.href = data.orderUrl;
                },
                fail(err) {
                    console.error(err);
                }
            });

        } else { // Если редактирование
            $(this).request('onUpdateOrder', {
                data,
                success({ address_from, address_to }) {
                    $modal.modal('hide');
                    $('.order__address-from').text(address_from);
                    $('.order__address-to').text(address_to);
                },
                fail(err) {
                    console.error(err);
                }
            });
        }
    });

    $('.orders__delete').on('click', function(e) {
        e.preventDefault();

        const btn = this;

        $(this).request('onDeleteOrder', {
            confirm: 'Вы действительно хотите удалить заказ?',
            data: {
                id: this.dataset.id,
            },
            success(data) {
                $(btn).closest('.orders-item').remove();

                if ($('.orders .orders__item').length === 0) {
                    $('.orders > .orders__no-data').show();
                }
            },
        });
    });

    $('.callback-btn').on('click', function() {
        $('#modal-contact').modal();
    });

    $('#contact-form').on('submit', function(e) {
        e.preventDefault();

        $(this).request('onSubmitContactForm', {
            data: {
                test: 'test',
            },
            success(res) {
                $.oc.flashMsg({
                    'text': 'Спасибо, выше сообщение успешно отправлено.',
                    'class': 'success',
                    'interval': 3
                });
            },
            error(err) {
                if (err.responseJSON && err.responseJSON.X_OCTOBER_ERROR_FIELDS && err.responseJSON.X_OCTOBER_ERROR_FIELDS.email) {
                    $.oc.flashMsg({
                        'text': err.responseJSON.X_OCTOBER_ERROR_FIELDS.email[0],
                        'class': 'error',
                        'interval': 3
                    });
                }
            },
        });
    });

    // Очищать модалку при закрытии
    $modal.on('hidden.bs.modal', function(e) {
        $modal.find('.modal-title').empty();
        $modal.find('.modal-body').empty();
    });
});
